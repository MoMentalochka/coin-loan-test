

export const state = () => ({
  commissions: [1, 2, 3, 4, 5],
  currencyList: [
    {
      name: 'Australian Dollar',
      code: 'AUD',
    },
    {
      name: 'Euro',
      code: 'EUR',
    },
    {
      name: 'Azerbaijanian Manat',
      code: 'AZN',
    },
    {
      name: 'Lek',
      code: 'ALL',
    },
    {
      name: 'Algerian Dinar',
      code: 'DZD',
    },
    {
      name: 'US Dollar',
      code: 'USD',
    },
    {
      name: 'Kwanza',
      code: 'AOA',
    },
    {
      name: 'Argentine Peso',
      code: 'ARS',
    },
    {
      name: 'Taka',
      code: 'BDT',
    },
    {
      name: 'Boliviano',
      code: 'BOB',
    }
  ],
  currencyPairs: []
})

export const mutations = {
  setCurrencyPairs(state, data) {
    console.log('gen')
    state.currencyPairs = data
  }
}

export const actions = {
  generateCurrencyPairs({commit, state}) {
    let result = []
    state.currencyList.forEach((item, idx, array) => {
      let itemPairs = []
      for(let i = idx + 1; i < array.length; i++) {

        itemPairs.push({base_currency: item.code, quote_currency: array[i]?.code, commission: state.commissions[ Math.floor(Math.random() * state.commissions.length)]})
      }
      result = result.concat(itemPairs)
    })
    commit('setCurrencyPairs', result)
  }
}
export const getters = {
  currencies(state) {
    return state.currencyList
  },
  getCurrencyPairs(state) {
    return state.currencyPairs
  },

  exchangeRates(state) {
    return state.currencyPairs.map(item => {
      return { pair: [item.base_currency, item.quote_currency], rate: Math.floor(Math.random() * 100) }
    })
  }
}
